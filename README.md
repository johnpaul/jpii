# Pocket John Paul II

Feel like Pope John Paul II was with you everywhere, ready to party at any time.

## Known issues

Currently, it seems that the firmware v1.2.0 cannot handle installation of applications with package size > ~1MB using the App Store. For now, it's advised to install the app manually by copying the zip contents to `/flash/sys/apps/johnpaul-jpii`.

## Features

- Background drums sequence with configurable BPMs (top and bottom-left petals) and toggling (the bottom-right petal)
- LEDs blinking to the rhythm with _Vatican_ white and yellow back and forth
- JPII automatically bouncing to the rhythm
- JPII bouncing as you wish (use the right shoulder petal to tell him how to do it)
- JPII's favorite melody playing under your finger tip (press the left shoulder petal to play the next note)
- Inspiring quotes (just press any of the white petals)