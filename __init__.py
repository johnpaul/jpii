import st3m.run, random, leds
from st3m.application import Application, ApplicationContext
from st3m.input import InputState, InputController
from ctx import Context
from st3m.goose import Tuple, Iterator

import cmath
import math
import bl00mbox

blm = bl00mbox.Channel("JPII")

YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)

class jp2(Application):
    pad_state_to_frame = {
        (True, False, False): 3,
        (True, True, False): 2,
        (False,True, False): 7,
        (False, True, True): 6,
        (False, False, True): 5,
        (True, False, True): 4,
        (True, True, True): 1,
    }

    barka_melody = [
        # Verse
        "B4", "A4", "B4", "C5", "B4", "A4", "G4", "G4", 
        "A4", "B4", "C5", "C5", "C5", "C5", "C5", "B4", "A4", "A4",
        "D4", "G4", "A4", "B4", "B4", "A4", "B4", "A4", "G4", "G4",
        # Chorus 
        "G4", "E5", "E5", "E5", "Gb5", "G5", "Gb5", "E5", "D5", "D5",
        "C5", "B4", "C5", "C5", "C5", "D5", "E5", "D5", "C5", "B4", "B4", 
        "G4", "G4", "E5", "E5", "E5", "Gb5", "G5", "Gb5", "E5", "D5", "D5", 
        "C5", "B4", "C5", "C5", "A4", "B4", "C5", "B4", "A4", "G4"
    ]

    def __init__(self, context):
        super().__init__(context)
        self.current_frame = 1
        self.max_frames = 7
        self.current_led_color = YELLOW
        self.since_last_beat_ms = 0
        self.current_note_idx = 0
        self.bpm = 90
        self.period_ms = 1/(self.bpm / 60 / 1000)
        self.drums_on = True

        leds.set_all_rgb(*WHITE)
        leds.update()

        self.synth = blm.new(bl00mbox.patches.tinysynth_fm)
        self.synth.signals.output = blm.mixer

        self.samples = [None] * 8
        self.samples_loaded = False
        self.samples_loader = self.load_samples()
        self.samples_loaded_count = 0
        self.samples_loading_text = "..."

        # For some unknown reason it has to be 1 track more than used, otherwise it crashes.
        self.seq = blm.new(bl00mbox.patches.sequencer, num_tracks=4, num_steps=8) 
        self.seq.signals.bpm.value = self.bpm
    
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if not self.samples_loaded and self.continue_loading_samples():
            return
            
        self.calculate_since_last_beat(delta_ms)
        self.calculate_next_animation_frame(ins)
        self.calculate_leds_state()
        self.calculate_bpm()        
        
        self.play_drums()
        self.play_samples()
        self.play_melody()

    def load_samples(self) -> Iterator[Tuple[int, str]]:
        yield 0, "kick.wav"
        self.samples[2] = blm.new(bl00mbox.patches.sampler, "kick.wav")
        self.samples[2].signals.output = blm.mixer
        self.samples[2].signals.trigger = self.seq.plugins.seq.signals.track0

        yield 1, "snare.wav"
        self.samples[3] = blm.new(bl00mbox.patches.sampler, "snare.wav")
        self.samples[3].signals.output = blm.mixer
        self.samples[3].signals.trigger = self.seq.plugins.seq.signals.track1

        yield 2, "hihat.wav"
        self.samples[4] = blm.new(bl00mbox.patches.sampler, "hihat.wav")
        self.samples[4].signals.output = blm.mixer
        self.samples[4].signals.trigger = self.seq.plugins.seq.signals.track2

        yield 3, "jeszcze-jak.wav"
        self.samples[0] = blm.new(bl00mbox.patches.sampler, "/sys/apps/johnpaul-jpii/samples/jeszcze-jak.wav")
        self.samples[0].signals.output = blm.mixer

        yield 4, "mozna.wav"
        self.samples[1] = blm.new(bl00mbox.patches.sampler, "/sys/apps/johnpaul-jpii/samples/mozna.wav")
        self.samples[1].signals.output = blm.mixer
        
        yield 5, "co.wav"
        self.samples[5] = blm.new(bl00mbox.patches.sampler, "/sys/apps/johnpaul-jpii/samples/co.wav")
        self.samples[5].signals.output = blm.mixer
        
        yield 6, "z-warkoczykami.wav"
        self.samples[6] = blm.new(bl00mbox.patches.sampler, "/sys/apps/johnpaul-jpii/samples/z-warkoczykami.wav")
        self.samples[6].signals.output = blm.mixer
        
        yield 7, "albo-bez.wav"
        self.samples[7] = blm.new(bl00mbox.patches.sampler, "/sys/apps/johnpaul-jpii/samples/albo-bez.wav")
        self.samples[7].signals.output = blm.mixer

    def continue_loading_samples(self) -> bool:
        try:
            self.samples_loaded_count, self.samples_loading_text = next(self.samples_loader)
            return True
        except StopIteration:
            self.samples_loaded = True
            return False

    def calculate_since_last_beat(self, delta_ms: int) -> None:
        self.since_last_beat_ms += delta_ms
        if self.since_last_beat_ms  > self.period_ms and self.period_ms > 0:
            self.since_last_beat_ms = 0
    
    def calculate_next_animation_frame(self, ins: InputState) -> None:
        petal = ins.captouch.petals[2] 
        if petal.pressed:
            pads = petal.pads.base, petal.pads.ccw, petal.pads.cw
            self.current_frame = self.pad_state_to_frame[pads]
        elif self.since_last_beat_ms == 0 and self.drums_on:
            if self.current_frame == 4:
                self.current_frame = 6
            else:
                self.current_frame = 4
    
    def calculate_leds_state(self):
        if self.since_last_beat_ms == 0:
            if self.current_led_color == YELLOW:
                self.current_led_color = WHITE
            else:
                self.current_led_color = YELLOW

    def calculate_bpm(self) -> None:
        if self.input.captouch.petals[0].whole.pressed:
            self.bpm += 5
            if self.bpm > 200:
                self.bpm = 200
        
        if self.input.captouch.petals[6].whole.pressed:
            self.bpm -= 5
            if self.bpm < 50:
                self.bpm = 50
        
        self.period_ms = 2/(self.bpm / 60 / 1000) if self.bpm != 0 else 0

    def play_samples(self):
        if self.input.captouch.petals[1].whole.pressed:
            self.samples[0].signals.trigger.start()
        
        if self.input.captouch.petals[3].whole.pressed:
            self.samples[1].signals.trigger.start()
        
        if self.input.captouch.petals[5].whole.pressed:
            self.samples[5].signals.trigger.start()
        
        if self.input.captouch.petals[7].whole.pressed:
            self.samples[6].signals.trigger.start()
        
        if self.input.captouch.petals[9].whole.pressed:
            self.samples[7].signals.trigger.start()
    
    def play_drums(self):
        if self.input.captouch.petals[4].whole.pressed:
            self.drums_on = not self.drums_on
        
        if self.drums_on and self.samples_loaded:
            vol = 32767
            self.seq.trigger_start(0, 0, vol)
            self.seq.trigger_start(1, 2, vol)
            self.seq.trigger_start(2, 3, vol)
            self.seq.trigger_start(1, 4, vol)
            self.seq.trigger_start(2, 5, vol)
            self.seq.trigger_start(1, 6, vol)
            self.seq.signals.bpm.value = self.bpm
        else:
            self.seq.signals.bpm.value = 0

    def play_melody(self) -> None:        
        input = self.input
        melody_petal = input.captouch.petals[8].whole
        if melody_petal.pressed:
            note = self.barka_melody[self.current_note_idx]
            self.synth.signals.pitch.tone = note
            self.synth.signals.trigger.start()
        
        if melody_petal.released:
            self.current_note_idx += 1
            if self.current_note_idx >= len(self.barka_melody):
                self.current_note_idx = 0
            self.synth.signals.trigger.stop() 

    def draw(self, ctx: Context) -> None:
        if not self.samples_loaded:
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()            
            ctx.text_align = ctx.MIDDLE
            ctx.font_size = 15
            ctx.move_to(0, -10)
            ctx.rgb(0.8, 0.8, 0.8)
            ctx.text(f"Loading {self.samples_loading_text}...")

            ctx.move_to(0, -40)
            progress = self.samples_loaded_count / len(self.samples)
            ctx.rgb(*YELLOW).rectangle(-80, 0, 160 * progress, 10).fill()
            return

        ctx.image_smoothing = False

        ctx.rgb(255, 255, 0).rectangle(-120, -120, 240, 240).fill()
        theframe = f"/flash/sys/apps/johnpaul-jpii/frames/frame_{self.current_frame}.png"
        ctx.image(theframe,-120,-120,240,240)
        
        leds.set_all_rgb(*(self.current_led_color))
        leds.update()

if __name__ == "__main__":
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(jp2(ApplicationContext()))
